# Title Maker

This is a quick little web tool that allows the user to set the title of the page. I use it to create named tabs for organizing the plethora of tabs i keep open. Works really well with the [Tree Tab Style](https://piro.sakura.ne.jp/xul/_treestyletab.html.en) plugin.
